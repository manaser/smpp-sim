# smpp-sim

Open-smpp simulator (SMSC) with wiremock for storing the messages. The messages can then be retrieved via REST API with soapUI for example.